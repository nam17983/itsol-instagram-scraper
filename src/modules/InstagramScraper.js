const InstagramProfileScraper = require('./InstagramProfileScraper')
const InstagramPostScraper = require('./InstagramPostScraper')
const fs = require(`fs`)

class InstagramScraper {

    constructor(path = `instagram`, number_post = 5, host = `https://instagram.com/`) {
        this.path = path;
        this.number_post = number_post;
        this.host = host;
    }

    async start() {
        let profileScraper = new InstagramProfileScraper(this.path)
        let user = await profileScraper.getUserProfile();
        // console.log(user);

        let instagramPostScraper = new InstagramPostScraper(this.path, this.number_post);
        let posts = await instagramPostScraper.getPostsInfo();
        // console.log("Done posts");
        // console.log(posts);

        this.buildJSON(user, posts);

        process.exit();
    }

    buildJSON(user, posts) {
        let tmp = {
           "profile" : user,
           "posts" : {
               "total" : posts.length,
               "data" : posts
           }               
        };
        fs.writeFileSync('./data_output/data_scraper.json', JSON.stringify(tmp, null, 4));
    }
}

module.exports = InstagramScraper
