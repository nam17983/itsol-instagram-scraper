const request = require('request-promise');
const cheerio = require('cheerio');

/* Create the base function to be ran */
class InstagramDetailPostScraper {

    constructor(path = `instagram`) {
        this.path = path
    }

    async getDetailPost() {
        /* Here you replace the username with your actual instagram username that you want to check */
        const BASE_URL = this.path;

        /* Send the request and get the html content */
        let response = await request(
            BASE_URL,
            {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
                'cache-control': 'max-age=0',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
            }
        );
        
        /* Initiate Cheerio with the response */
        let $ = cheerio.load(response);

        /* Get the proper script of the html page which contains the json */
        let script = $('script').eq(3).html();

        /* Traverse through the JSON of instagram response */
        let { entry_data: { PostPage : {[0] : { graphql : {shortcode_media} }} } } = JSON.parse(/window\._sharedData = (.+);/g.exec(script)[1]);
        
        return this.extractUsefulData(shortcode_media);
    }

    extractUsefulData(shortcode_media) {
        delete shortcode_media.dimensions;
        delete shortcode_media.gating_info;
        delete shortcode_media.media_preview;
        delete shortcode_media.display_resources;
        delete shortcode_media.is_video;
        delete shortcode_media.should_log_client_event;
        delete shortcode_media.tracking_token;
        delete shortcode_media.edge_media_to_tagged_user;
        delete shortcode_media.caption_is_edited;
        delete shortcode_media.has_ranked_comments;
        delete shortcode_media.comments_disabled;
        delete shortcode_media.edge_media_to_sponsor_user;
        delete shortcode_media.location;
        delete shortcode_media.viewer_has_liked;
        delete shortcode_media.viewer_has_saved;
        delete shortcode_media.viewer_has_saved_to_collection;
        delete shortcode_media.viewer_in_photo_of_you;
        delete shortcode_media.viewer_can_reshare;
        delete shortcode_media.is_ad;
        delete shortcode_media.edge_web_media_to_related_media;
        delete shortcode_media.edge_sidecar_to_children;
        delete shortcode_media.owner;
        
        return shortcode_media;
    }
}

module.exports = InstagramDetailPostScraper