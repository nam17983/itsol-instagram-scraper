const request = require('request-promise');
const cheerio = require('cheerio');
const ora = require(`ora`)
const chalk = require(`chalk`)

/* Create the base function to be ran */
class InstagramProfileScraper {

    constructor(path = `instagram`) {
        this.path = path
        this.spinner = ora().start();
    }

    async getUserProfile() {
        /* Here you replace the username with your actual instagram username that you want to check */
        // const USERNAME = 'lisamendedesign';
        const BASE_URL = `https://www.instagram.com/${this.path}/`;

        /* Send the request and get the html content */
        let response = await request(
            BASE_URL,
            {
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
                'cache-control': 'max-age=0',
                'upgrade-insecure-requests': '1',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
            }
        );
        
        /* Initiate Cheerio with the response */
        let $ = cheerio.load(response);

        /* Get the proper script of the html page which contains the json */
        let script = $('script').eq(4).html();
        
        /* Traverse through the JSON of instagram response */
        let { entry_data: { ProfilePage : {[0] : { graphql : {user} }} } } = JSON.parse(/window\._sharedData = (.+);/g.exec(script)[1]);
        
        this.spinner.succeed(chalk.green(`Scraped ${BASE_URL} profile successed`))

        return this.extractUsefulData(user);
    }

    extractUsefulData(user) {
        user.edge_owner_to_timeline_media_post_count = user.edge_owner_to_timeline_media.count;

        delete user.edge_owner_to_timeline_media;
        delete user.edge_saved_media;
        delete user.edge_media_collections;
        delete user.edge_felix_video_timeline;
        delete user.connected_fb_page;
        delete user.requested_by_viewer;
        delete user.is_verified;
        delete user.is_private;
        delete user.is_joined_recently;
        delete user.is_business_account;
        delete user.has_requested_viewer;
        delete user.highlight_reel_count;
        delete user.has_blocked_viewer;
        delete user.has_channel;
        delete user.follows_viewer;
        delete user.followed_by_viewer;
        delete user.blocked_by_viewer;
        delete user.country_block;
        delete user.edge_mutual_followed_by;

        return user;
    }
}

module.exports = InstagramProfileScraper