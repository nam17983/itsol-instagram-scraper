const puppeteer = require(`puppeteer`)
const ora = require(`ora`)
const chalk = require(`chalk`)
const InstagramDetailPostScraper = require('./InstagramDetailPostScraper')

class InstagramPostScraper {

    constructor(path = `instagram`, number_post = 5, host = `https://instagram.com/`) {
        this.path = path;
        this.number_post = number_post;
        this.host = host;
        this.spinner = ora().start();
    }

    get url() {
        return `${this.host}${this.path}`
    }

    async getPostsInfo() {
        this.spinner.text = chalk.yellow(`Scraping url: ${this.url}`)
        this.browser = await puppeteer.launch()
        this.page = await this.browser.newPage()
        
        await this.page.setExtraHTTPHeaders({
            'Accept-Language': 'en-US'
        })

        await this.page.goto(this.url, {
            waitUntil: `networkidle0`
        })

        if (await this.page.$(`.dialog-404`)) {
            this.spinner.fail(`The url you followed may be broken`);
            process.exit()
        }

        this.spinner.succeed(chalk.green(`Valid page found`))
        this.spinner.start()
        return this.evaluate()
    }

    async evaluate() {
        try {
            this.items = await this.load(this.number_post)

            this.posts = await this.loadDetailPost(this.items);
        } catch (error) {
            this.spinner.fail(`Error: ${error}`)
            this.spinner.fail(`There was a problem parsing the page`)
            process.exit()
        }
        this.spinner.succeed(chalk.green(`Scraped ${this.items.size} posts`))
        await this.page.close()
        await this.browser.close()

        return this.posts;
    }

    async load(maxItemsSize) {
        this.maxItemsSize = maxItemsSize
        var page = this.page
        let previousHeight
        var media = new Set()
        var index = `.`

        while (maxItemsSize == null || media.size < maxItemsSize) {
            try {
                previousHeight = await page.evaluate(`document.body.scrollHeight`)
                await page.evaluate(`window.scrollTo(0, document.body.scrollHeight)`)
                await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`)
                await page.waitFor(1000)
                this.spinner.text = chalk.yellow(`Scrolling${index}`)

                const nodes = await page.evaluate(() => {
                    // const images = document.querySelectorAll(`a > div > div.KL4Bh > img`)
                    // return [].map.call(images, img => img.src)
                    const links = document.querySelectorAll(`div.v1Nh3 > a`)
                    return [].map.call(links, link => link.href)
                })

                nodes.forEach(element => {
                    if (media.size < maxItemsSize) {
                        media.add(element)
                    }
                })
                
                index = index + `.`
            }
            catch (error) {
                break
            }
        }
        return media
    }

    async loadDetailPost(urls) {
        let posts = [];
        urls.forEach(item => {
            let instagramDetailPostScraper = new InstagramDetailPostScraper(item);
            let postDetail = instagramDetailPostScraper.getDetailPost();
            posts.push(postDetail);
        })
        return await Promise.all(posts);
    }
}

module.exports = InstagramPostScraper
