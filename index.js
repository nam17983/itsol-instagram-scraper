const InstagramScraper = require('./src/modules/InstagramScraper')

let path = process.argv[2]
let number_post = process.argv[3]
let instagramScraper = new InstagramScraper(path, number_post)
instagramScraper.start().catch(error => console.error(error))